module PlasmaViz

using Requires
using LaTeXStrings
using ColorSchemes
using StructArrays
using CoordinateTransformations
using Makie

# Import the MHD equilibrium packages
using PlasmaEquilibriumToolkit

function __init__()
  #@require GLMakie="e9467ef8-e4e7-5192-8a1a-b1aee30e663a" include("include_GLMakie.jl")

  #@require CairoMakie="13f3f980-e62b-5c42-98c6-ff1f3baf88f0" include("include_CairoMakie.jl")

  @require VMEC="2b46c670-0004-47b5-bf0a-1741584931e9" begin
    include("VMEC.jl")
    include("VMEC/Fieldline.jl")
    include("VMEC/RadialProfile.jl")
    include("VMEC/Surface2D.jl")
    include("VMEC/Surface3D.jl")
    include("VMEC/vmecSummaryPlotting.jl")
  end
end

end
