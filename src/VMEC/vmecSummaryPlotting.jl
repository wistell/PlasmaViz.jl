function vmec_summary_plot(eq::Vmec;
                           size::Tuple{Int,Int}=(1200,800),
                           fontsize::Int=20,
                           font = "new computer modern latin",
                           title::Union{Nothing,AbstractString,LaTeXString}=nothing,
                          )
    fig = Figure(size = size,fontsize=fontsize, font = font)
    bmag = fig[1,1] = GridLayout()
    xc = fig[1,2] = GridLayout()
    booz = fig[2,1] = GridLayout()
    iota = fig[2,2] = GridLayout()
    nfp = eq.nfp
    last_surface = (eq.ns - 1)/eq.ns
    angles = LinRange(0,2π,128)
    bmag_surface_plot!(bmag, eq, last_surface, angles, angles)
    toroidal_planes = [(i-1)*π/nfp/4 for i in 1:5]
    flux_cross_section_plot!(xc, eq, last_surface, toroidal_planes; fontsize = fontsize)
    plot_Boozer_Modes!(booz, eq, m=24, n=24)
    rot_transf_plot!(iota, eq; fontsize = fontsize)
    colsize!(fig.layout, 1, Auto(2.0))
    titleStr = isnothing(title) ? "" : title 
    Label(fig[1,1:2,Top()],titleStr, valign = :bottom, padding=(0,0,5,0))
    fig
end

    
