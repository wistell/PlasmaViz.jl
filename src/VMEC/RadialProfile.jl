"""
    rot_transf_plot(vmec::Vmec; plotDeriv = false, clearFigure = true)

Plots the rotational transform `ι` w.r.t. the normalized toroidal flux `s`.
If `plot_deriv = true`, the derivative of `ι` w.r.t. `s` is plotted.
# Example
```julia-repl
julia> using NetCDF, PlasmaEquilibriumToolkit, VMEC
julia> wout = NetCDF.open("/path/to/your/vmec/file.nc") #change to your personal path
julia> vmec, vmecdata = VMEC.readVmecWout(wout);
julia> rot_transf_plot(vmec)
```
**If you want to plot multiple functions next to each other in the same figure
set clearFigure = false**
"""
function rot_transf_plot(vmec::Vmec;
                         plotDeriv::Bool=false,
                         title::Union{Nothing,AbstractString} = nothing,
                         resolution::Tuple{Int,Int}=(600,400),
                         fontsize = 20,
                         label = "",
                        )
  fig = Figure(resolution = resolution,fontsize=20, font = "new computer modern latin")
  theme = Theme(font = "new computer modern latin", fontsize = fontsize)
  figlayout = fig[1,1] = GridLayout()
  rot_transf_plot!(figlayout,vmec,plotDeriv=plotDeriv,
                   title=title, fontsize = fontsize, label = label)
  fig
end

function rot_transf_plot!(fig::GridLayout,
                          vmec::Vmec;
                          plotDeriv::Bool=false,
                          clearFigure::Bool = false,
                          title::String = "",
                          fontsize = 20,
                          label = "",
                         )
    sMin = vmec.iota.itp.ranges[1][1];
    sMax = vmec.iota.itp.ranges[1][end];
    nRes = 1000;
    sHires = LinRange(sMin,sMax,nRes);
    plotData = !plotDeriv ? vmec.iota[sHires] : map(x->Interpolations.gradient(vmec.iota, x)[1], sHires)
    xlabelStr = "s"
    ylabelStr = !plotDeriv ? "ι(s)" : "dι/ds"
    titleStr = title
    rowPosition, colPosition = (1,1)
    if clearFigure || typeof(current_figure()) == Nothing
      fig = Figure()
    #else
    #  fig = current_figure()
    #  rowPosition, colPosition = indexing(fig)
    end
    ax = Axis(fig[rowPosition,colPosition], xlabel=xlabelStr, ylabel=ylabelStr, title=titleStr, titlefont = "new computer modern latin", titlesize = fontsize, xlabelsize = fontsize, ylabelsize = fontsize, limits = ((0.0, 1.0), (0.95 * minimum(plotData), 1.05*maximum(plotData))))
    lines!(ax, sHires, plotData , color=:black, label = label)
    fig
end

function rot_transf_overplot!(fig,
                              vmec::Vmec;
                              plotDeriv::Bool=false,
                              fontsize = 20,
                              label = "",
                              plot_label = false,
                             )
    sMin = vmec.iota.itp.ranges[1][1];
    sMax = vmec.iota.itp.ranges[1][end];
    nRes = 1000;
    sHires = LinRange(sMin,sMax,nRes);
    plotData = !plotDeriv ? vmec.iota[sHires] : map(x->Interpolations.gradient(vmec.iota, x)[1], sHires)
    ax = Axis(fig[1, 1])
    hidexdecorations!(ax)
    hideydecorations!(ax)
    lines!(ax, sHires, plotData , color=:black, linestyle = :dash, label = label)
    if plot_label
        axislegend(ax, position = :lt)
    end
    fig
end

"""
    plotBoozerModes(eq::Vmec; m = 16, n = 16, numberOfModes = 10)

Plots magnitude of the magnetic field 'Bₘₙ' of vmec data for the
largest boozer modes (at s=1) w.r.t. the normalized toroidal flux `s`.
# Example
```julia-repl
julia> using NetCDF, PlasmaEquilibriumToolkit, VMEC
julia> wout = NetCDF.open("/path/to/your/vmec/file.nc") #change to your personal path
julia> vmec, vmecdata = VMEC.readVmecWout(wout);
julia> plotBoozerModes(vmec)
```
**If you want to plot multiple functions next to each other in the same figure
set clearFigure = false**
"""
function plot_Boozer_Modes(eq::Vmec;
                           m = 16,
                           n = 16,
                           numberOfModes = 10,
                           title::Union{Nothing,AbstractString}=nothing,
                          )
  fig = Figure(resolution = (1200,800),fontsize=20, font = "new computer modern latin")
  figlayout = fig[1,1] = GridLayout()
  plot_Boozer_Modes!(figlayout,eq,m=m,n=n,numberOfModes=numberOfModes,
                     title=title)
  fig
end

function plot_Boozer_Modes!(fig::GridLayout,
                            eq::Vmec;
                            m = 16,
                            n = 16,
                            numberOfModes = 10,
                            title::Union{Nothing,AbstractString}=nothing
                           )
    s = 1.0/eq.ns:1.0/eq.ns:(eq.ns-1)/eq.ns
    wholeSpectrum = VMEC.boozer_fourier_spectrum(:bmn,s,eq,m,n)
    lastSurf = wholeSpectrum[end]
    amp = map(x->x.cos,lastSurf)
    ampInds = reverse(sortperm(abs.(amp)))[2:2+numberOfModes]
    newM = map(x->x.m,lastSurf[ampInds])
    newN = map(x->x.n,lastSurf[ampInds])
    fullAmps = Matrix{Float64}(undef,length(s),numberOfModes);
    
    ax = Axis(fig[1,1], xlabel="s", ylabel="Bₘₙ", titlefont = "new computer modern latin")
    for j = 1:numberOfModes
        for (i,s) in enumerate(s)
            fullAmps[i,j] = wholeSpectrum[i][ampInds[j]].cos
        end
        m , n = Int(newM[j]), Int(newN[j])
        lines!(ax, s, fullAmps[:,j], label="(m,n)=($m,$n)")
    end
    #axislegend(position = :lb)
    leg = Legend(fig[1,2],ax)
    fig
end
