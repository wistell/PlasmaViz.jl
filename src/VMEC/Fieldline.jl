"""
    field_line_plot(eq::Vmec,fluxCoords::StructArray{FluxCoordinates};clearFigure = true)

Plots a single flux coordinate field line in Cartesian coordinates
# Example
```julia-repl
julia> using NetCDF, PlasmaEquilibriumToolkit, VMEC
julia> wout = NetCDF.open("/path/to/your/vmec/file.nc") #change to your personal path
julia> vmec, vmecdata = VMEC.readVmecWout(wout);
julia> ζs = LinRange(0,2π,500);
julia> ψ = 3;
julia> θ = π;
julia> fluxCoords = MagneticCoordinateGrid(FluxCoordinates, ψ, θ, ζs);
julia> flux_line_plot(vmec, fluxCoords)
```
**If you want to plot multiple functions next to each other in the same figure
set clearFigure = false**
"""
function field_line_plot(vmec::Vmec,
                         center::PestCoordinates{T, T},
                         span::T = T(2π);
                         title::Union{Nothing,AbstractString} = nothing,
                         resolution::Tuple{Int,Int}=(600,400),
                         fontsize = 20,
                         label = "",
                         with_wireframe::Bool = true,
                        ) where {T}
    fig = Figure(resolution = resolution,fontsize=20, font = "new computer modern latin")
    fig_layout = fig[1,1] = GridLayout()
    field_line_plot!(fig_layout, vmec, center, span;
                     title = title, fontsize = fontsize, label = label,
                     with_wireframe = with_wireframe)
    return fig
end

function field_line_plot!(fig::GridLayout,
                          vmec::Vmec,
                          center::PestCoordinates{T,T},
                          span::T;
                          title::String = "",
                          fontsize = 20,
                          label = "",
                          with_wireframe::Bool = true,
                          clear_figure::Bool = false,
                         ) where {T}
    s₀ = center.ψ*2π/vmec.phi(1.0)*vmec.signgs
    α₀ = center.α
    ζ₀ = center.ζ
    θ₀ = α₀ + vmec.signgs * vmec.iota(s₀) * ζ₀
    half_span = span / 2
    n_turns = round(Int, span / 2 / π)
    n_points = n_turns * 256 + 1
    ζs = range(start = ζ₀ - half_span, stop = ζ₀ + half_span, length = n_points)
    fl_pest_coords = MagneticCoordinateCurve(PestCoordinates, s₀, α₀, ζs)
    vmec_surface = VmecSurface(s₀, vmec)
    fl_cart_coords = CartesianFromPest()(fl_pest_coords, vmec_surface);
    #shortS = round(s, digits = 3)
    #shortθ = round(θ, digits = 3)
    #firstZeta = round(flux_coords.ζ[1], digits=3)
    #lastZeta = round(flux_coords.ζ[end], digits=3)
    fl_X = [q[1] for q in fl_cart_coords];
    fl_Y = [q[2] for q in fl_cart_coords];
    fl_Z = [q[3] for q in fl_cart_coords];
    (wf_X, wf_Y, wf_Z) = if with_wireframe
        wf_flux_coords = MagneticCoordinateGrid(FluxCoordinates, s₀, range(start = 0.0, stop = 2π, length = 16), range(start = 0.0, stop = 2π, length = 128))
        wf_cart_coords = CartesianFromFlux()(wf_flux_coords, vmec_surface);
        ([q[1] for q in wf_cart_coords], [q[2] for q in wf_cart_coords], [q[3] for q in wf_cart_coords])
    else
        (nothing, nothing, nothing)
    end
    rowPosition, colPosition = (1,1)
    if clear_figure || typeof(current_figure()) == Nothing
        fig = Figure()
    #else
    #    fig = current_figure()
    #    rowPosition, colPosition = indexing(fig)
    end
    ax = Axis3(fig[rowPosition, colPosition], azimuth = vmec.signgs * ζ₀)
    if with_wireframe
        wireframe!(ax, wf_X, wf_Y, wf_Z, color = :lightgrey, linewidth = 0.5)
    end
    lines!(fig[rowPosition,colPosition], fl_X,fl_Y,fl_Z, color = ζs, linewidth = 4, colormap = :twilight)
    #title = fig[rowPosition - 1,colPosition] = Label(fig, "FluxCoordinate curve for s=$shortS, θ=$shortθ, $firstZeta<ζ< $lastZeta",tellwidth=false)
    fig
end

"""
    pest_line_plot(eq::Vmec,fluxCoords::StructArray{PestCoordinates};clearFigure = true)

Plots  a single pest coordinate field line in cartesian coords.
# Example
```julia-repl
julia> using NetCDF, PlasmaEquilibriumToolkit, VMEC
julia> wout = NetCDF.open("/path/to/your/vmec/file.nc") #change to your personal path
julia> vmec, vmecdata = VMEC.readVmecWout(wout);
julia> ζs = LinRange(0,4π,500);
julia> ψ = 3;
julia> α = π;
julia> pestCoords = MagneticCoordinateGrid(PestCoordinates, ψ, α, ζs);
julia> pest_line_plot(vmec, pestCoords)
```
**If you want to plot multiple functions next to each other in the same figure
set clearFigure = false**
"""
function pest_line_plot(eq::Vmec{T},
                        pestCoords::StructArray{PestCoordinates{T,T},N,C,I};
                        clearFigure = true,
                       ) where {T, N, C <: Union{Tuple,NamedTuple}, I}
  @assert ndims(pestCoords) == 1 "Input coordinates must be a StructVector"
  s = pestCoords[1,1].ψ*2π/eq.phi(1.0)*eq.signgs
  α = pestCoords[1].α
  vmecSurface = VmecSurface(s, eq)
  vmecCoords = VmecFromPest()(pestCoords, vmecSurface);
  cartCoords = CartesianFromVmec()(vmecCoords, vmecSurface);
  X = [q[1] for q in cartCoords];
  Y = [q[2] for q in cartCoords];
  Z = [q[3] for q in cartCoords];
  shortS = round(s, digits = 3)
  shortα = round(α, digits = 3)
  firstZeta = round(pestCoords[1].ζ, digits=3)
  lastZeta = round(pestCoords[end].ζ, digits=3)
  rowPosition, colPosition = (1,1)
  titleStr = "Magnetic field line with s=$shortS, α=$shortα, $firstZeta<ζ< $lastZeta"
  if clearFigure || typeof(current_figure()) == Nothing
      fig = Figure()
  else
      fig = current_figure()
      rowPosition, colPosition = indexing(fig)
  end
  lscene = fig[rowPosition,colPosition] = LScene(fig)
  lines!(fig[rowPosition,colPosition], X,Y,Z, colormap = :viridis)
  title = fig[rowPosition - 1,colPosition] = Label(fig, titleStr, tellwidth=false)
  fig
end


