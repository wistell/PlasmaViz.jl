"""
    flux_surface_plot(eq::Vmec,fluxCoords::StructArray{FluxCoordinates};clearFigure::Bool = true)

Plots a 3d Flux surface in cartesian coordinates.
# Example
```julia-repl
julia> using NetCDF, PlasmaEquilibriumToolkit, VMEC
julia> wout = NetCDF.open("/path/to/your/vmec/file.nc") #change to your personal path
julia> vmec, vmecdata = VMEC.readVmecWout(wout);
julia> angles = LinRange(0,2π,500);
julia> ψ = 3;
julia> fluxCoords = MagneticCoordinateGrid(FluxCoordinates, ψ, angles, angles);
julia> flux_surface_plot(vmec, fluxCoords)
```
**If you want to plot multiple functions next to each other in the same figure
set clearFigure = false**
"""
function flux_surface_plot(eq::Vmec{T},
                           fluxCoords::StructArray{FluxCoordinates{T,T},N,C,I};
                           clearFigure::Bool = true) where {T, N, C<:Union{Tuple,NamedTuple},I}
    s = fluxCoords.ψ[1,1]*2π/eq.phi(1.0)*eq.signgs
    vmecSurface = VmecSurface(s, eq);
    vmecCoords = VmecFromFlux()(fluxCoords, vmecSurface);
    cartCoords = CartesianFromVmec()(vmecCoords, vmecSurface);
    X = [q[1] for q in cartCoords];
    Y = [q[2] for q in cartCoords];
    Z = [q[3] for q in cartCoords];
    shortS = round(s, digits=3)
    firstTheta = round(fluxCoords.θ[1,1], digits = 2)
    lastTheta = round(fluxCoords.θ[1,end], digits = 2)
    firstZeta = round(fluxCoords.ζ[1,1], digits = 2)
    lastZeta = round(fluxCoords.ζ[end,1], digits = 2)
    fig = nothing
    lscene = nothing
    rowPosition, colPosition = (1,1)
    titleStr = "Flux surface for s=$shortS, $firstTheta<θ<$lastTheta,and $firstZeta<ζ<$lastZeta"
    if clearFigure || typeof(current_figure()) == Nothing
        fig = Figure()
    else
        fig = current_figure()
        rowPosition, colPosition = indexing(fig)
    end
    lscene = LScene(fig[rowPosition,colPosition])
    surface!(lscene, X,Y,Z, colormap = :viridis)
    title = fig[rowPosition-1,colPosition] = Label(fig,titleStr,tellwidth=false)
    fig
end

"""
    flux_cross_section_plot(eq::Vmec, s, toroidal_planes = [0.])

Plots the cross sections of the magnetic surface at position `s` at each
of the planes in `toroidal_planes`.
# Example
```julia-repl
julia> using NetCDF, PlasmaEquilibriumToolkit, VMEC

julia> wout = "/path/to/your/vmec/file.nc";

julia> vmec = VMEC.readVmecWout(wout);

julia> fig = flux_cross_section_plot(vmec, 0.5, [0., π/4/vmec.nfp, π/2/vmec.nfp, 3π/4/vmec.nfp])
```
"""
function flux_cross_section_plot(eq::Vmec{T},
                              s,
                              toroidal_planes = [0.];
                              nθ = 128,
                              fig_size = (1200, 800),
                              fontsize = 24,
                              title = "",
                             ) where {T}
    vmecSurface = VmecSurface(s, eq);
    ψ = s * eq.phi(1.0)/(2π) * eq.signgs
    fig = Figure(size = fig_size, fontsize = fontsize, font = "new computer modern latin")
    ax = Axis(fig[1, 1], xlabel = "R (m)", ylabel = "Z (m)", title = title, titlefont = "new computer modern latin", aspect = DataAspect())

    θ_points = range(start = 0.0, stop = 2π, length = nθ)
    R = Vector{T}(undef, length(θ_points))
    Z = similar(R)
    for (i, ϕ) in enumerate(toroidal_planes)
        
        points = [FluxCoordinates(ψ, θ, ϕ) for θ in θ_points]
        RZ = CylindricalFromFlux()(points, vmecSurface)
        map!(q -> q.r, R, RZ)
        map!(q -> q.z, Z, RZ)
        lines!(ax, R, Z, linestyle=:solid, linewidth=4, label="ζ = $(round(ϕ,digits=3))", colormap = :Dark2_8)
    end
    axislegend(ax)
    fig
end

function flux_cross_section_plot(eq::Vmec{T},
                                 fluxCoords::StructArray{FluxCoordinates{T,T},N,C,I};
                                 title::Union{Nothing,AbstractString}=nothing,
                                 fontsize = 20,
                                 resolution = (600, 600),
                                ) where {T, N, C <: Union{Tuple,NamedTuple}, I}
  fig = Figure(resolution=resolution,fontsize=20, font = "new computer modern latin")
  theme = Theme(fontsize = fontsize, font = "new computer modern latin")
  set_theme!(theme)
  figlayout = fig[1,1] = GridLayout()
  flux_cross_section_plot!(figlayout,eq,fluxCoords;title=title, fontsize = fontsize)
  return fig
end

function flux_cross_section_plot!(fig::GridLayout,
                                  eq::Vmec{T},
                                  s,
                                  toroidal_planes = [0.];
                                  nθ = 128,
                                  title::Union{Nothing,AbstractString}=nothing,
                                  fontsize = 24,
                                 ) where {T, N, C <: Union{Tuple,NamedTuple}, I}
    vmecSurface = VmecSurface(s, eq);
    ψ = s * eq.phi(1.0)/(2π) * eq.signgs
    shortS = round(s, digits=3)
    titleStr = isnothing(title) ? "Cross-sections at s=$shortS" : title #from ζ=$startζ->$endζ"
    linestyles = [:solid,:dash,:dot,:dashdot,:dashdotdot]
    ax = Axis(fig[1, 1], xlabel = "R (m)", ylabel = "Z (m)", title = titleStr, titlefont = "new computer modern latin", aspect = DataAspect())

    θ_points = range(start = 0.0, stop = 2π, length = nθ)
    R = Vector{T}(undef, length(θ_points))
    Z = similar(R)
    for (i, ϕ) in enumerate(toroidal_planes)
        points = [FluxCoordinates(ψ, θ, ϕ) for θ in θ_points]
        RZ = CylindricalFromFlux()(points, vmecSurface)
        map!(q -> q.r, R, RZ)
        map!(q -> q.z, Z, RZ)
        lines!(ax, R, Z, linestyle=linestyles[mod1(i, 5)], linewidth=4, label="ζ = $(round(ϕ,digits=3))", colormap = :Dark2_8)
    end
    #axislegend(ax)
end

function flux_cross_section_overplot!(fig,
                                      eq::Vmec{T},
                                      fluxCoords::StructArray{FluxCoordinates{T,T},N,C,I};
                                     ) where {T, N, C <: Union{Tuple,NamedTuple}, I}
  s = fluxCoords.ψ[1,1]*2π/eq.phi(1.0)*eq.signgs
  vmecSurface = VmecSurface(s, eq);

  axis = Axis(fig[1, 1])  
  hidexdecorations!(axis)
  hideydecorations!(axis)
  for i = 1:size(fluxCoords,1)
    cylinderCoords = CylindricalFromFlux()(view(fluxCoords,i,:), vmecSurface);
    r = [q.r for q in cylinderCoords]
    z = [q.z for q in cylinderCoords]
    lines!(axis, r, z, linestyle=:dot, linewidth=4, colorscheme = :Dark2_8)#color=colors[((i-1) % length(colors))+1])
    #lines!(axis, r, z, linestyle=linestyles[((i-1) % 5)+1], linewidth=4, label="ζ = $(round(fluxCoords[i,1].ζ,digits=3))",color=colors[((i-1) % 5)+1])
  end
  #axislegend(position = :lt)
  fig
end

function cross_section_grid_plot(eq::Vmec{T},
                                 n_surfaces::Int = 10,
                                 n_θ::Int = 8,
                                 ζ::T = 0.0;
                                 res::Tuple{Int, Int} = (1200, 800),
                                 fontsize::Int = 20,
                                 title::String = "",
                                 line_points::Int = 128,
                                ) where {T}
  
  fig = Figure(resolution = res, fontsize = fontsize, font = "new computer modern latin")
  ax = fig[1, 1] = Axis(fig, xlabel = "R (m)", y = "Z (m)", title = title, titlefont = "new computer modern latin")
  s_values = range(0.5/(eq.ns-1), step=(1.0-1.0/(eq.ns-1))/(n_surfaces-1), length=n_surfaces)
  θ_values = range(0.0, step=2π/(n_θ), length=n_θ)
  s_grid = range(0.5/(eq.ns-1), step=(1.0-1.0/(eq.ns-1))/line_points, length=line_points+1)
  ψ_grid = map(s -> eq.phi(s)/(2π)*eq.signgs, s_grid)
  θ_grid = range(0.0, step = 2π/line_points, length=line_points+1)
  coord_grid = MagneticCoordinateGrid(FluxCoordinates, ψ_grid, θ_grid, ζ)
  R = Vector{T}(undef, length(θ_grid))
  Z = similar(R)
  θ_lines = Vector{Vector{Cylindrical{T,T}}}(undef, n_θ) 
  for (i, s) in enumerate(s_grid)
    surface = VmecSurface(s, eq)
    RZ = CylindricalFromFlux()(coord_grid[:, i], surface)
    map!(q -> q.r, R, RZ)
    map!(q -> q.z, Z, RZ)
    if any(s_v -> isapprox(s_v, s), s_values)
      lines!(ax, R, Z, linestyle = :dash, linewidth = 2, color = :red)
    end
    foreach(j -> push!(θ_lines[j], RZ[findfirst(k -> isapprox(k, θ_values[j]), θ_grid)]), eachindex(θ_values))
  end
end
  #=
  for (i, θ) in enumerate(θ_values)
    coord_curve = MagneticCoordinateCurve(FluxCoordinates, ψ_grid, θ, ζ)
    vmec_curve = VmecFromFlux()(coord_
    RZ = CylindricalFromFlux()(coord_curve,  
    =#

"""
Plots |B| on a magnetic flux surface.
# Example
```julia-repl
julia> using NetCDF, PlasmaEquilibriumToolkit, VMEC
julia> wout = NetCDF.open("/path/to/your/vmec/file.nc") #change to your personal path
julia> vmec, vmecdata = VMEC.readVmecWout(wout);
julia> angles = LinRange(0,2π,500);
julia> ψ = 3;
julia> fluxCoords = MagneticCoordinateGrid(FluxCoordinates, ψ, angles, angles);
julia> bmag_surface_plot(vmec,fluxCoords)
```
**If you want to plot multiple functions next to each other in the same figure
set clearFigure = false**
"""
function bmag_surface_plot(eq::Vmec{T},
                            s,
                            theta_range,
                            zeta_range,
                          ) where {T}
    vmecSurface = VmecSurface(s, eq);
    vmecCoords = coordinate_grid_2D(s, theta_range, zeta_range, eq)
    cartCoords = [CartesianFromVmec()(v, vmecSurface) for v in vmecCoords]
    X = [q[1] for q in cartCoords]
    Y = [q[2] for q in cartCoords]
    Z = [q[3] for q in cartCoords]
    Bmag = [VMEC.inverseTransform(v, vmecSurface.bmn) for v in vmecCoords]
    lscene = LScene(fig[1, 1])
    surf = surface!(lscene, X,Y,Z, color = [B for B in Bmag], colormap = :inferno)
    #title = Label(fig[rowPosition-1,colPosition],titleStr,tellwidth=false)
    legend = Colorbar(fig[1,2], surf, label = "|B|")
    fig
end

function bmag_surface_plot!(fig::GridLayout,
                            eq::Vmec,
                            s,
                            theta_range,
                            zeta_range,
                           )
    vmecSurface = VmecSurface(s, eq);
    vmecCoords = coordinate_grid_2D(s, theta_range, zeta_range, eq)
    cartCoords = [CartesianFromVmec()(v, vmecSurface) for v in vmecCoords]
    X = [q[1] for q in cartCoords]
    Y = [q[2] for q in cartCoords]
    Z = [q[3] for q in cartCoords]
    Bmag = [VMEC.inverseTransform(v, vmecSurface.bmn) for v in vmecCoords]
    lscene = LScene(fig[1, 1])
    surf = surface!(lscene, X,Y,Z, color = [B for B in Bmag], colormap = :inferno)
    #title = Label(fig[rowPosition-1,colPosition],titleStr,tellwidth=false)
    legend = Colorbar(fig[1,2], surf, label = "|B|")
    fig
end
