"""
Function used in all graphing functions, in order to index where graphs
should go in a window, if multiple graphs are plotted on the same window.
"""
function indexing(fig;
                  maxColumn::Int=5,
                 )
    layout = size(fig.layout)
    rowPosition = layout[1]
    colPosition = 1
    while true
        contents(fig[rowPosition, colPosition]) != Any[] ? colPosition += 1 : break
        if colPosition > maxColumn
            rowPosition += 2
            colPosition = 1
            break
        end
    end
    return rowPosition, colPosition
end
