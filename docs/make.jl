using PlasmaViz
using Documenter

DocMeta.setdocmeta!(PlasmaViz, :DocTestSetup, :(using PlasmaViz); recursive=true)

makedocs(;
    modules=[PlasmaViz],
    authors="Benjamin Faber <wistell@wisc.edu> and contributors",
    repo="https://gitlab.com/wistell/PlasmaViz.jl/blob/{commit}{path}#{line}",
    sitename="PlasmaViz.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://wistell.gitlab.io/PlasmaViz.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
