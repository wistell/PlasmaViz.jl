# PlasmaViz

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://wistell.gitlab.io/PlasmaViz.jl/dev)
[![Build Status](https://gitlab.com/wistell/PlasmaViz.jl/badges/master/pipeline.svg)](https://gitlab.com/wistell/PlasmaViz.jl/pipelines)
[![Coverage](https://gitlab.com/wistell/PlasmaViz.jl/badges/master/coverage.svg)](https://gitlab.com/wistell/PlasmaViz.jl/commits/master)

Visualization package for the [PlasmaEquilibriumToolkit](https://gitlab.com/wistell/PlasmaEquilibriumToolkit.jl)
